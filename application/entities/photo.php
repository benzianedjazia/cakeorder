<?php

class photo{
    private $id;
    private $photo;
    private $path;

    public function __construct(array $params) {
     $this->hydrate($params);

    }

    function hydrate($params) {
        foreach ($params as $key => $value) {
            $method = "set" . ucfirst($key);
            if (method_exists($this, $method)) {
                $this->$method($value);
            }
        }
    }

static function getRandomUrlFolder():string{
    return str_replace("/","\\",realpath(".")."/upload/");
}
    /*
     * Get the value of id_catalogue
     */ 


    /*
     * Get the value of photo
     */ 
    public function getphoto()
    {
        return $this->photo;
    }

    /*
     * Set the value of photo
     *
     * @return  self
     */ 
    public function setphoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }
    public function setpath($path)
    {
        $this->path = $path;

        return $this;
    }
    public function getpath()
    {
        return $this->path;
    }

    /*
     * @return mixed
     */


    /* 
     * @param mixed $id 
     * @return catalogue
     */

    /*
     * @return mixed
     */
    function getId() {
        return $this->id;
    }

    /**
     * @param mixed $id 
     * @return catalogue
     */
    function setId($id): self {
        $this->id = $id;
        return $this;
    }
}

?>
