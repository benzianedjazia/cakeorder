<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Commande
 *
 * @author didie
 */

use application\libraries\loaderFile;

require_once "application/libraries/loaderFile.php";

abstract class File {

    static private $_tabAcceptFormt = ["image/jpeg", "image/jpg", "image/png"];

    use LoaderFile;

    static function saveFile($superGlobalFile) {
        if (isset($superGlobalFile)):
            $nbImg = count($superGlobalFile["photo_donut"]["name"]);
            for ($cmpt = 0; $cmpt < $nbImg; $cmpt++):
                if (!($superGlobalFile["photo_donut"]["error"][$cmpt] > 0)):
                    if ($superGlobalFile["photo_donut"]["size"][$cmpt] < 500000):
                        if (in_array($superGlobalFile["photo_donut"]["type"][$cmpt], self::$_tabAcceptFormt)):
                            $extention_uploaded = strtolower(substr(strchr($superGlobalFile["photo_donut"]["name"][$cmpt], "."), 1));
                            var_dump(getimagesize($superGlobalFile["photo_donut"]["tmp_name"][$cmpt]));
                            if (getimagesize($superGlobalFile["photo_donut"]["tmp_name"][$cmpt])[0] < 550 && getimagesize($superGlobalFile["photo_donut"]["tmp_name"][$cmpt])[1] < 250):
                                $completPath = self::getRandomUrlFolder() . self::getRandomName() . ".".$extention_uploaded;
                                if (move_uploaded_file($superGlobalFile["photo_donut"]["tmp_name"][$cmpt], $completPath)):
                                    return $completPath;
                                else:
                                    return ["flag_error" => true];
                                endif;
                            else:
                                return "fichier trop grand en taille!";
                            endif;
                        else:
                            return 'Format de fichier non autorisé';
                        endif;
                    else:
                        return 'Le fichier est trop volumineux';
                    endif;
                else:
                    return 'Erreur lors du chargement';
                endif;
            endfor;
        endif;
    }

    static function getRandomUrlFolder(): string {
        return str_replace("/", "\\", realpath(".") . "/upload/");
    }
    static function saveFileByAjax($superGlobalFile) {
        if (isset($superGlobalFile)):
           
                if (!($superGlobalFile["photo_donut"]["error"] > 0)):
                    if ($superGlobalFile["photo_donut"]["size"] < 500000):
                        if (in_array($superGlobalFile["photo_donut"]["type"], self::$_tabAcceptFormt)):
                            $extention_uploaded = strtolower(substr(strchr($superGlobalFile["photo_donut"]["name"], "."), 1));
                            var_dump(getimagesize($superGlobalFile["photo_donut"]["tmp_name"]));
                            if (getimagesize($superGlobalFile["photo_donut"]["tmp_name"])[0] < 550 && getimagesize($superGlobalFile["photo_donut"]["tmp_name"])[1] < 250):
                                $completPath = self::getRandomUrlFolder() . self::getRandomName() . ".jpeg";
                                if (move_uploaded_file($superGlobalFile["photo_donut"]["tmp_name"], $completPath)):
                                    return $completPath;
                                else:
                                    return ["flag_error" => true];
                                endif;
                            else:
                                return "fichier trop grand en taille!";
                            endif;
                        else:
                            return 'Format de fichier non autorisé';
                        endif;
                    else:
                        return 'Le fichier est trop volumineux';
                    endif;
                else:
                    return 'Erreur lors du chargement';
                endif;
          
        endif;
    }
}
