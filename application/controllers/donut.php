<?php

namespace application\controllers;

//header('Content-Type: application/json');

include_once '../libraries/autoloader_.php';

include_once '../libraries/autoloader.php';

function extractDatasFromForm($superGlobal): array {
    $datas = null;
    foreach ($superGlobal as $key => $value) {
        $datas[str_replace("_donut", "", $key)] = $value;
    }
    return $datas;
}

function sp_global_extract_datas_with_pattern(array $super_gb, array $tab_patterns) {
    $tab = $super_gb;
    foreach ($tab_patterns as $pattern) {
        foreach ($tab as $key => $value) {
            if (preg_match($pattern, $key)):
                $tab[preg_replace($pattern, "", $key)] = trim($value);
                unset($tab[$key]);
            endif;
        }
    }
    return $tab;
}

function ajoute(): void {
    if (isset($_POST)) {
        if(!$feedback["flag_error"]):
           $photoDonut=new photo(["path"=>$path]);
           ele
        $datas = sp_global_extract_datas_with_pattern($_POST, ["/_donut/"]);
        
        $donut = new \donut($datas);
        $PDOFactory = new \PDOFactory("mysql:host=localhost;dbname=cakeorder", $user = "root", $pwd = "", true);
        $donutManager = new \donutManager($PDOFactory->getPDO());

        $response = $donutManager->add($donut);

        echo json_encode($response);
    }
}

if (isset($_POST['action'])) :
    switch ($_POST['action']):
        case "ajoute":
            ajoute();
            break;
        case "suppression":
            break;
        default:
            \Route::defaultRedirection();
            break;
    endswitch;

else:
    \Route::defaultRedirection();
endif;


