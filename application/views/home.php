<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>cakeorder.fr</title>
    <base href="http://www.cakeorderold.fr">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="../../assets/style.css">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Oswald:wght@500;600;700&family=Pacifico&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->



    <!-- Libraries Stylesheet -->
    <link href="../../assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- PERSONNALISÉized Bootstrap Stylesheet -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js" integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous"></script>
</head>
<style>
    html,
    body {
        width: 100%;
        min-height: 100%;
    }

    main {
        min-width: 100%;
        min-height: 100%;
    }

    #ctn_div_central {
        width: 100%;
        padding: 0px;
    }

    section[id^="sctn_tab_"] {
        width: 100%;
        padding: 0px 80px;
        margin: 40px 0px;
    }

    /* .ms-auto {
                    /* margin-right: 50px !important;
                }*/

    nav.mr-2 {
        margin-right: 20px !important;
    }


    /* 
                .dsply-no{
                   display: none;
                            }*/

    #ss_ctn_from {
        display: table;
        width: 100%;
        border-spacing: 25px;
    }

    #ss_ctn_from_left {
        display: table-cell;
        width: 50%;
    }

    .dsply-no {
        display: none !important;
    }

    #slct_cvlt {
        width: 145px;
    }

    input[type=date] {
        width: 150px;
    }

    .header {
        background-color: #100f10 !important;
        box-shadow: 1px 1px 3px #0c0b0d94;
    }



    #ctn_form_search {
        width: 500px !important;
    }

    #output {
        width: 100% !important;
    }

    /* configuration des fenêtres modales */
    .modal-header {
        padding: 0.5rem 0.5rem !important;
    }

    /*Réajustement de positionnement de la fenêtre modal au centre */
    #dialogInfo .modal-dialog {
        position: absolute;
        width: 100% !important;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    /*Ajustement modal-body*/

    .modal-body {
        padding: 2rem 1rem !important;
    }



    /*Ajustement sur le tableau de résultat*/

    #tbl_result tbody tr:hover {
        background-color: #fdd;
        transition-delay: 0s;
        transition-duration: 1s;
        transition-property: all;
    }

    #tbl_result tr {
        text-align: center;
        cursor: pointer;
    }


    /*Surcharge de la classe active*/
    .active {
        background-color: #bd0dfd !important;
    }


    #ss_ctn_from legend {
        text-align: center;
        text-transform: uppercase;
    }

    /**/
    .form-control:focus {
        box-shadow: none !important;
        border-color: silver;
    }

    /**/
    button.btn:focus {
        box-shadow: none !important;
        border-color: silver;
    }


    /*Ajust first li navbar*/

    ul.navbar-nav:first-child {
        padding: 0.65rem !important;
    }

    .navbar {
        padding: 0px;
    }

    /*Ajustement du ctn li de session*/
    #no_load {
        margin-left: 10px !important;
        width: 160px !important;
    }
</style>

<body>
    <!-- Topbar Start -->
    <div id="topbar" class="container-fluid px-0 d-none d-lg-block">
        <div class="row gx-0">
            <div class="col-lg-4 text-center bg-secondary py-3">
                <div class="d-inline-flex align-items-center justify-content-center">
                    <i class="bi bi-envelope fs-1 text-primary me-3"></i>
                    <div class="text-start">
                        <h6 class="text-uppercase mb-1">ENVOYEZ-NOUS UN EMAIL</h6>
                        <span>info@example.com</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center bg-primary border-inner py-3">
                <div class="d-inline-flex align-items-center justify-content-center">
                    <a href="index.html" class="navbar-brand"> 
                        <h1 class="m-0 text-uppercase text-white"><img src="../../assets/img/img - Copie/elyasdonutslogo.jpg" width="50px" style="transform:translate(-50px,-5px) ;border-radius:50% ;"  alt=""></i>ELYA'S DONUTS</h1>
                    </a>
                </div>
            </div>
            <div class="col-lg-4 text-center bg-secondary py-3">
                <div class="d-inline-flex align-items-center justify-content-center">
                    <i class="bi bi-phone-vibrate fs-1 text-primary me-3"></i>
                    <div class="text-start">
                        <h6 class="text-uppercase mb-1">APPELEZ-NOUS</h6>
                        <span>+012 345 6789</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->
    <div id="ctn_div_central">
        <header class="navbar navbar-expand-md navbar-dark header">
             <section class="navbar-brand">
      
                <img src="../../assets/img/img - Copie/elyasdonutslogo.jpg" width="60px" style="border-radius:50% ;"  alt="">
                <span id="" class="narbar-text ml-3">ELYA'S DONUTS</span>
            </section> 
            
            <section class="navbar ms-auto ">
                <nav class="mr-1">
                    <ul class="navbar-nav nav-pills justify-content-center ">
                        <li data-sctn-id="1" class="nav-item" id="dashbord1">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/home.php">Accueil</a>
                        </li>
                        <li data-sctn-id="6" class="nav-item">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/avis.php">avis</a>
                        </li>
                        <li data-sctn-id="6" class="nav-item">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/contact.php">Contact</a>
                        </li>
                        <li data-sctn-id="6" class="nav-item">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/inscription.php">inscription</a>
                        </li>
                        
                        <li data-sctn-id="6" class="nav-item">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/authentification.php">connexion</a>
                        </li>

                        <!-- <li class="nav-item dropdown" id="no_load">
                                Il faut retirer la classe .dropdown-toggle de a pour enlever > 
                                <a class="nav-link bg-light  " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span class="bg-light text-black p-1 rounded"><?= $_SESSION["admin_name"]; ?></span>
                                </a>
                                <ul id="menu_prcpl" class="dropdown-menu dropdown" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="#">Paramètres</a></li>
                                    <li id="btn_actn_logout"><a class="dropdown-item" href="#">Déconnexion</a></li>
                                </ul>
                            </li> -->
                    </ul>

                </nav>
            </section>
        </header>

    </div>


    <section id="dashbord" data-sctn-id="1" id="sctn_tab_1">
        <!-- Hero Start -->
        <div class="container-fluid bg-primary py-5 mb-5 hero-header">
            <div class="container py-5">
                <div class="row justify-content-start">
                    <div class="col-lg-8 text-center text-lg-start">
                        <h1 class="font-secondary text-primary mb-4">Super DONUTS</h1>
                        <h1 class="display-1 text-uppercase text-white mb-4">ELYA'S DONUTS</h1>
                        <h1 class="text-uppercase text-white">The Best Cake In TouLouSe</h1>
                        <div class="d-flex align-items-center justify-content-center justify-content-lg-start pt-5">
                            <a href="" class="btn btn-primary border-inner py-3 px-5 me-5">Read More</a>
                            <button type="button" class="btn-play" data-bs-toggle="modal" data-src="https://www.youtube.com/embed/DWRcNpR6Kdc" data-bs-target="#videoModal">
                                <span></span>
                            </button>
                            <h5 class="font-weight-normal text-white m-0 ms-4 d-none d-sm-block">Play Video</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Hero End -->
        <!-- About Start -->
        <div class="container-fluid pt-5">
            <div class="container">
                <div class="section-title position-relative text-center mx-auto mb-5 pb-3" style="max-width: 600px;">
                    <h2 class="text-primary font-secondary">About Us</h2>
                    <h1 class="display-4 text-uppercase">Welcome To ELYA'SDONUTS</h1>
                </div>
                <div class="row gx-5">
                    <div class="col-lg-5 mb-5 mb-lg-0" style="min-height: 400px;">
                        <div class="position-relative h-100">
                            <img class="position-absolute w-100 h-100" src="../../assets/img/12.jpg" style="object-fit: cover;">
                        </div>
                    </div>
                    <div class="col-lg-6 pb-5">
                        <h4 class="mb-4">Tempor erat elitr rebum clita. Diam dolor diam ipsum erat lorem sed stet labore lorem sit clita duo</h4>
                        <p class="mb-5">Tempor erat elitr at rebum at at clita. Diam dolor diam ipsum et tempor sit. Clita erat ipsum et lorem et sit, sed stet no labore lorem sit. Sanctus clita duo justo et tempor eirmod magna dolore erat amet magna</p>
                        <div class="row g-5">
                            <div class="col-sm-6">
                                <div class="d-flex align-items-center justify-content-center bg-primary border-inner mb-4" style="width: 90px; height: 90px;">
                                    <i class="fa fa-heartbeat fa-2x text-white"></i>
                                </div>
                                <h4 class="text-uppercase">100% Healthy</h4>
                                <p class="mb-0">Labore justo vero ipsum sit clita erat lorem magna clita nonumy dolor magna dolor vero</p>
                            </div>
                            <div class="col-sm-6">
                                <div class="d-flex align-items-center justify-content-center bg-primary border-inner mb-4" style="width: 90px; height: 90px;">
                                    <i class="fa fa-award fa-2x text-white"></i>
                                </div>
                                <h4 class="text-uppercase">Award Winning</h4>
                                <p class="mb-0">Labore justo vero ipsum sit clita erat lorem magna clita nonumy dolor magna dolor vero</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- About End -->

        <!-- Facts Start -->
        <div class="container-fluid bg-img py-5 mb-5">
            <div class="container py-5">
                <div class="row gx-5 gy-4">
                    <div class="col-lg-3 col-md-6">
                        <div class="d-flex">
                            <div class="bg-primary border-inner d-flex align-items-center justify-content-center mb-3" style="width: 60px; height: 60px;">
                                <i class="fa fa-star text-white"></i>
                            </div>
                            <div class="ps-4">
                                <h6 class="text-primary text-uppercase">NOTRE EXPÉRIENCE</h6>
                                <h1 class="display-5 text-white mb-0" data-toggle="counter-up">2 ans </h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="d-flex">
                            <div class="bg-primary border-inner d-flex align-items-center justify-content-center mb-3" style="width: 60px; height: 60px;">
                                <i class="fa fa-users text-white"></i>
                            </div>
                            <div class="ps-4">
                                <h6 class="text-primary text-uppercase">SPÉCIALISTE DES GÂTEAUX</h6>
                                <h1 class="display-5 text-white mb-0" data-toggle="counter-up">12345</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="d-flex">
                            <div class="bg-primary border-inner d-flex align-items-center justify-content-center mb-3" style="width: 60px; height: 60px;">
                                <i class="fa fa-check text-white"></i>
                            </div>
                            <div class="ps-4">
                                <h6 class="text-primary text-uppercase">PROJET COMPLET</h6>
                                <h1 class="display-5 text-white mb-0" data-toggle="counter-up">12345</h1>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6">
                        <div class="d-flex">
                            <div class="bg-primary border-inner d-flex align-items-center justify-content-center mb-3" style="width: 60px; height: 60px;">
                                <i class="fa fa-mug-hot text-white"></i>
                            </div>
                            <div class="ps-4">
                                <h6 class="text-primary text-uppercase">CLIENTS HEUREUX</h6>
                                <h1 class="display-5 text-white mb-0" data-toggle="counter-up">12345</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Facts End -->


        <div class="container-fluid about py-5">
            <div class="container">
                <div class="section-title position-relative text-center mx-auto mb-5 pb-3" style="max-width: 600px;">
                    <h2 class="text-primary font-secondary">Menus & Tarifs</h2>
                    <h1 class="display-4 text-uppercase">DÉCOUVREZ NOS GÂTEAUX</h1>
                </div>
                <div class="tab-class text-center">
                    <ul class="nav nav-pills d-inline-flex justify-content-center bg-dark text-uppercase border-inner p-4 mb-5">
                        <li class="nav-item">
                            <a class="nav-link text-white active" data-bs-toggle="pill" href="#tab-1">ANNIVERSAIRE</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" data-bs-toggle="pill" href="#tab-2">GÂTEAU DE MARIAGE</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" data-bs-toggle="pill" href="#tab-3">PERSONNALISÉ</a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div id="tab-1" class="tab-pane fade show p-0 active" style="background: #FAF3EB;">
                            <div class="row g-3" style="background: #FAF3EB;">
                                <div class="col-lg-6" style="background: #FAF3EB;">
                                    <div class="d-flex h-100">
                                        <div class="flex-shrink-0">
                                            <img class="img-fluid" src="../../assets/img/cake-1.jpg" alt="" style="width: 150px; height: 85px;">
                                            <h4 class="bg-dark text-primary p-2 m-0">$99.00</h4>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center text-start bg-secondary border-inner px-4">
                                            <h5 class="text-uppercase">GÂTEAU D'ANNIVERSAIRE</h5>
                                            <span>Ipsum ipsum clita erat amet dolor sit justo sea eirmod diam stet sit justo</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="d-flex h-100">
                                        <div class="flex-shrink-0">
                                            <img class="img-fluid" src="../../assets/img/cake-1.jpg" alt="" style="width: 150px; height: 85px;">
                                            <h4 class="bg-dark text-primary p-2 m-0">$99.00</h4>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center text-start bg-secondary border-inner px-4">
                                            <h5 class="text-uppercase">GÂTEAU D'ANNIVERSAIRE</h5>
                                            <span>Ipsum ipsum clita erat amet dolor sit justo sea eirmod diam stet sit justo</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="d-flex h-100">
                                        <div class="flex-shrink-0">
                                            <img class="img-fluid" src="../../assets/img/cake-1.jpg" alt="" style="width: 150px; height: 85px;">
                                            <h4 class="bg-dark text-primary p-2 m-0">$99.00</h4>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center text-start bg-secondary border-inner px-4">
                                            <h5 class="text-uppercase">GÂTEAU D'ANNIVERSAIRE</h5>
                                            <span>Ipsum ipsum clita erat amet dolor sit justo sea eirmod diam stet sit justo</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="d-flex h-100">
                                        <div class="flex-shrink-0">
                                            <img class="img-fluid" src="../../assets/img/cake-1.jpg" alt="" style="width: 150px; height: 85px;">
                                            <h4 class="bg-dark text-primary p-2 m-0">$99.00</h4>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center text-start bg-secondary border-inner px-4">
                                            <h5 class="text-uppercase">GÂTEAU D'ANNIVERSAIRE</h5>
                                            <span>Ipsum ipsum clita erat amet dolor sit justo sea eirmod diam stet sit justo</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                        <div id="tab-2" class="tab-pane fade show p-0">
                            <div class="row g-3" style="background: #FAF3EB;">


                                <div class="col-lg-6">
                                    <div class="d-flex h-100">
                                        <div class="flex-shrink-0">
                                            <img class="img-fluid" src="../../assets/img/cake-2.jpg" alt="" style="width: 150px; height: 85px;">
                                            <h4 class="bg-dark text-primary p-2 m-0">$99.00</h4>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center text-start bg-secondary border-inner px-4">
                                            <h5 class="text-uppercase">GÂTEAU DE MARIAGE </h5>
                                            <span>Ipsum ipsum clita erat amet dolor sit justo sea eirmod diam stet sit justo</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="d-flex h-100">
                                        <div class="flex-shrink-0">
                                            <img class="img-fluid" src="../../assets/img/cake-2.jpg" alt="" style="width: 150px; height: 85px;">
                                            <h4 class="bg-dark text-primary p-2 m-0">$99.00</h4>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center text-start bg-secondary border-inner px-4">
                                            <h5 class="text-uppercase">GÂTEAU DE MARIAGE </h5>
                                            <span>Ipsum ipsum clita erat amet dolor sit justo sea eirmod diam stet sit justo</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="tab-3" class="tab-pane fade show p-0">
                            <div class="row g-3" style="background: #FAF3EB;">
                                <div class="col-lg-6">
                                    <div class="d-flex h-100">
                                        <div class="flex-shrink-0">
                                            <img class="img-fluid" src="../../assets/img/cake-3.jpg" alt="" style="width: 150px; height: 85px;">
                                            <h4 class="bg-dark text-primary p-2 m-0">$99.00</h4>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center text-start bg-secondary border-inner px-4">
                                            <h5 class="text-uppercase">PERSONNALISÉ Cake</h5>
                                            <span>Ipsum ipsum clita erat amet dolor sit justo sea eirmod diam stet sit justo</span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="d-flex h-100">
                                        <div class="flex-shrink-0">
                                            <img class="img-fluid" src="../../assets/img/cake-3.jpg" alt="" style="width: 150px; height: 85px;">
                                            <h4 class="bg-dark text-primary p-2 m-0">$99.00</h4>
                                        </div>
                                        <div class="d-flex flex-column justify-content-center text-start bg-secondary border-inner px-4">
                                            <h5 class="text-uppercase">PERSONNALISÉ Cake</h5>
                                            <span>Ipsum ipsum clita erat amet dolor sit justo sea eirmod diam stet sit justo</span>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
        <!-- Products End -->
        <!-- Team Start -->
        <div class="container-fluid py-9">
            <div class="container">
                <div class="section-title position-relative text-center mx-auto mb-5 pb-3" style="max-width: 600px;">
                    <h2 class="text-primary font-secondary">Membres de l'équipe</h2>
                    <h1 class="display-8 text-uppercase">NOS MAÎTRES CUISINIERS</h1>
                </div>
                <div class="row g-5">
                    <div class="col-lg-6 col-md-6" style="display:table-cell; width:50%">
                        <div class="team-item">
                            <div class="position-relative overflow-hidden">
                                <img class="img-fluid w-100" src="../../assets/img/team-1.jpg" alt="">
                                <div class="team-overlay w-100 h-100 position-absolute top-50 start-50 translate-middle d-flex align-items-center justify-content-center">
                                    <div class="d-flex align-items-center justify-content-start">
                                        <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 mx-1" href="#"><i class="fab fa-twitter fw-normal"></i></a>
                                        <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 mx-1" href="#"><i class="fab fa-facebook-f fw-normal"></i></a>
                                        <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 mx-1" href="#"><i class="fab fa-linkedin-in fw-normal"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="bg-dark border-inner text-center p-4">
                                <h4 class="text-uppercase text-primary">Full Name</h4>
                                <p class="text-white m-0">Designation</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-6" style="display:table-cell; width:50%">
                        <div class="team-item">
                            <div class="position-relative overflow-hidden">
                                <img class="img-fluid w-100" src="../../assets/img/team-3.jpg" alt="">
                                <div class="team-overlay w-100 h-100 position-absolute top-50 start-50 translate-middle d-flex align-items-center justify-content-center">
                                    <div class="d-flex align-items-center justify-content-start">
                                        <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 mx-1" href="#"><i class="fab fa-twitter fw-normal"></i></a>
                                        <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 mx-1" href="#"><i class="fab fa-facebook-f fw-normal"></i></a>
                                        <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 mx-1" href="#"><i class="fab fa-linkedin-in fw-normal"></i></a>
                                    </div>
                                </div>
                            </div>
                            <div class="bg-dark border-inner text-center p-4">
                                <h4 class="text-uppercase text-primary">Full Name</h4>
                                <p class="text-white m-0">Designation</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <!-- Products Start -->


    <!-- Team End -->



    <!-- Footer Start -->
    <div class="container-fluid bg-img text-secondary" style="    margin-top: 132px;">
        <div class="container">
            <div class="row gx-5">
                <div class="col-lg-4 col-md-6 mb-lg-n5">
                    <div class="d-flex flex-column align-items-center justify-content-center text-center h-100 bg-primary border-inner p-4">
                        <a href="index.html" class="navbar-brand">
                            <h1 class="m-0 text-uppercase text-white"><i class="fa fa-birthday-cake fs-1 text-dark me-3"></i>ELYA'S DONUTS</h1>
                        </a>
                        <p class="mt-3">Lorem diam sit erat dolor elitr et, diam lorem justo labore amet clita labore stet eos magna sit. Elitr dolor eirmod duo tempor lorem, elitr clita ipsum sea. Nonumy rebum et takimata ea takimata amet gubergren, erat rebum magna lorem stet eos. Diam amet et kasd eos duo dolore no.</p>
                    </div>
                </div>
                <div class="col-lg-8 col-md-6">
                    <div class="row gx-5">
                        <div class="col-lg-4 col-md-12 pt-5 mb-5">
                            <h4 class="text-primary text-uppercase mb-4">ENTRER EN CONTACT</h4>
                            <div class="d-flex mb-2">
                                <i class="bi bi-geo-alt text-primary me-2"></i>
                                <p class="mb-0">123 Street, New York, USA</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-envelope-open text-primary me-2"></i>
                                <p class="mb-0">info@example.com</p>
                            </div>
                            <div class="d-flex mb-2">
                                <i class="bi bi-telephone text-primary me-2"></i>
                                <p class="mb-0">+33 345 67890</p>
                            </div>
                            <div class="d-flex mt-4">
                                <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-twitter fw-normal"></i></a>
                                <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-facebook-f fw-normal"></i></a>
                                <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-linkedin-in fw-normal"></i></a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                            <h4 class="text-primary text-uppercase mb-4">LIENS RAPIDES</h4>
                            <div class="d-flex flex-column justify-content-start">

                                <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Dashboard</a>
                                <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Commandes</a>
                                <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Rechercher</a>
                                <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>catalogue</a>
                                <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Enregistrer</a>
                                <a class="text-secondary" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Contact Us</a>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                            <h4 class="text-primary text-uppercase mb-4">Newsletter</h4>
                            <p>Amet justo diam dolor rebum lorem sit stet sea justo kasd</p>
                            <form action="">
                                <div class="input-group">
                                    <input type="text" class="form-control border-white p-3" placeholder="Your Email">
                                    <button class="btn btn-primary">Sign Up</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid text-secondary py-4" style="background: #111111;">
        <div class="container text-center">
            <p class="mb-0">&copy; <a class="text-white border-bottom" href="#">Your Site Name</a>. All Rights Reserved.


        </div>
    </div>
    <!-- Footer End -->


























    <!-- JavaScript Libraries -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
    <script src="lib/easing/easing.min.js"></script>
    <script src="lib/waypoints/waypoints.min.js"></script>
    <script src="lib/counterup/counterup.min.js"></script>
    <script src="lib/owlcarousel/owl.carousel.min.js"></script>

    <!-- Template Javascript -->
    <script src="js/main.js"></script>
</body>

</html>