<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>cakeorder.fr</title>
    <base href="http://www.cakeorderold.fr">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <link rel="stylesheet" href="../../assets/style.css">

    <!-- Favicon -->
    <link href="img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Oswald:wght@500;600;700&family=Pacifico&display=swap" rel="stylesheet">

    <!-- Icon Font Stylesheet -->



    <!-- Libraries Stylesheet -->
    <link href="../../assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Template Stylesheet -->
    <link href="../../assets/css/style.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
    <script src="../../assets/js/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js" integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous"></script>
</head>
<style>
    html,
    body {
        width: 100%;
        min-height: 100%;
    }

    main {
        min-width: 100%;
        min-height: 100%;
    }

    #ctn_div_central {
        width: 100%;
        padding: 0px;
    }

    section[id^="sctn_tab_"] {
        width: 100%;
    
    }

    /* .ms-auto {
                    /* margin-right: 50px !important;
                }*/

    nav.mr-2 {
        margin-right: 20px !important;
    }


    /* 
                .dsply-no{
                   display: none;
                            }*/

    #ss_ctn_from {
        display: table;
        width: 100%;
        border-spacing: 25px;
    }

    #ss_ctn_from_left {
        display: table-cell;
        width: 50%;
    }

    .dsply-no {
        display: none !important;
    }

    #slct_cvlt {
        width: 145px;
    }

    input[type=date] {
        width: 150px;
    }

    .header {
        background-color: #100f10 !important;
        box-shadow: 1px 1px 3px #0c0b0d94;
    }


    #ctn_form_search {
        width: 500px !important;
    }

    #output {
        width: 100% !important;
    }

    /* configuration des fenêtres modales */
    .modal-header {
        padding: 0.5rem 0.5rem !important;
    }


    /*Réajustement de positionnement de la fenêtre modal au centre */
    #dialogInfo .modal-dialog {
        position: absolute;
        width: 100% !important;
        top: 50%;
        left: 50%;
        transform: translate(-50%, -50%);
    }

    /*Ajustement modal-body*/

    .modal-body {
        padding: 2rem 1rem !important;
    }



    /*Ajustement sur le tableau de résultat*/

    #tbl_result tbody tr:hover {
        background-color: #fdd;
        transition-delay: 0s;
        transition-duration: 1s;
        transition-property: all;
    }

    #tbl_result tr {
        text-align: center;
        cursor: pointer;
    }


    /*Surcharge de la classe active*/
    .active {
        background-color: #bd0dfd !important;
    }


    #ss_ctn_from legend {
        text-align: center;
        text-transform: uppercase;
    }

    /**/
    .form-control:focus {
        box-shadow: none !important;
        border-color: silver;
    }

    /**/
    button.btn:focus {
        box-shadow: none !important;
        border-color: silver;
    }


    /*Ajust first li navbar*/

    ul.navbar-nav:first-child {
        padding: 0.65rem !important;
    }

    .navbar {
        padding: 0px;
    }

    /*Ajustement du ctn li de session*/
    #no_load {
        margin-left: 10px !important;
        width: 160px !important;
    }
</style>

<body>
    <!-- Topbar Start -->
    <div id="topbar" class="container-fluid px-0 d-none d-lg-block">
        <div class="row gx-0">
            <div class="col-lg-4 text-center bg-secondary py-3">
                <div class="d-inline-flex align-items-center justify-content-center">
                    <i class="bi bi-envelope fs-1 text-primary me-3"></i>
                    <div class="text-start">
                        <h6 class="text-uppercase mb-1">ENVOYEZ-NOUS UN EMAIL</h6>
                        <span>info@example.com</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 text-center bg-primary border-inner py-3">
            <section class="navbar-brand">
      
      <img src="../../assets/img/img - Copie/elyasdonutslogo.jpg" width="60px" style="border-radius:50% ;"  alt="">
      <span id="" class="narbar-text ml-3">ELYA'S DONUTS</span>
  </section> 
            </div>
            <div class="col-lg-4 text-center bg-secondary py-3">
                <div class="d-inline-flex align-items-center justify-content-center">
                    <i class="bi bi-phone-vibrate fs-1 text-primary me-3"></i>
                    <div class="text-start">
                        <h6 class="text-uppercase mb-1">APPELEZ-NOUS</h6>
                        <span>+012 345 6789</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Topbar End -->
    <div id="ctn_div_central">
        <header class="navbar navbar-expand-md navbar-dark header">
        <section class="navbar-brand">
      
      <img src="../../assets/img/img - Copie/elyasdonutslogo.jpg" width="60px" style="border-radius:50% ;"  alt="">
      <span id="" class="narbar-text ml-3">ELYA'S DONUTS</span>
  </section> 
            <section class="navbar ms-auto ">
                <nav class="mr-1">
                    <ul class="navbar-nav nav-pills justify-content-center ">
                        <li data-sctn-id="1" class="nav-item" id="dashbord1">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/home.php">Accueil</a>
                        </li>
                        <li data-sctn-id="6" class="nav-item">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/avis.php">Avis</a>
                        </li>
                        <li data-sctn-id="6" class="nav-item">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/contact.php">Contact</a>
                        </li>
                        <li data-sctn-id="6" class="nav-item">
                            <a class="nav-link " href="http://www.cakeorderold.fr/application/views/authentification.php">connexion</a>
                        </li>
                        <!-- <li class="nav-item dropdown" id="no_load">
                                Il faut retirer la classe .dropdown-toggle de a pour enlever > 
                                <a class="nav-link bg-light  " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                                    <span class="bg-light text-black p-1 rounded"><?= $_SESSION["admin_name"]; ?></span>
                                </a>
                                <ul id="menu_prcpl" class="dropdown-menu dropdown" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="#">Paramètres</a></li>
                                    <li id="btn_actn_logout"><a class="dropdown-item" href="#">Déconnexion</a></li>
                                </ul>
                            </li> -->
                    </ul>

                </nav>
            </section>
        </header>

    </div>

    <!-- Team End -->
    <section data-sctn-id="3" >
        <form id="form_inscription" action="http://www.cakeorderold.fr/application/controllers/inscription.php" method="post" enctype="application/x-www-form-urlencoded">
            <div id="ss_ctn_from">
                <input type="hidden" name="action" value="inscription">
                <div id="ss_ctn_from_left" class="p-3 bg-light border  rounded">
                    <legend class="border-bottom " style="width: 100%;">Données client</legend>
                    <div class="mb-2">
                        <label class="form-label" for="nom_utlstr">Civilité</label>
                        <select class="form-select" id="slct_cvlt">
                            <option selected="selected">--- Choisir ---</option>
                            <option value="masculin">Mr</option>
                            <option value="feminin">Mme</option>
                        </select>
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="nom_utlstr">Nom</label>
                        <input class="form-control" type="text" id="nom_utlstr" name="nom_utlstr">
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="prenom_utlstr">Prénom</label>
                        <input class="form-control" type="text" id="prenom_utlstr" name="prenom_utlstr">
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="email_utlstr">Email</label>
                        <input class="form-control" type="text" id="email_utlstr" name="email_utlstr">
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="telephone_utlstr">Téléphone</label>
                        <input class="form-control" type="text" id="telephone_utlstr" name="telephone_utlstr">
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="dateAnniversaire_utlstr">Date anniversaire</label>
                        <input class="form-control" type="date" id="dateAnniversaire_utlstr" name="dateAnniversaire_utlstr">
                    </div>
                    </fieldset>
                </div>
                <div id="ss_ctn_from_right" class="p-3 bg-light border  rounded">
                    <legend class="border-bottom">Adresse client</legend>
                    <div class="mb-2">
                        <label class="form-label" for="numVoie_utlstr">Numero de voie</label>
                        <input class="form-control" type="text" id="numVoie_utlstr" name="numVoie_utlstr">
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="libelleVoie_utlstr">Libelle de voie</label>
                        <input class="form-control" type="text" id="libelleVoie_utlstr" name="libelleVoie_utlstr">
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="codePostal_utlstr">Code postal</label>
                        <input class="form-control" type="text" id="codePostal_utlstr" name="codePostal_utlstr">
                    </div>
                    <div class="mb-2">
                        <label class="form-label" for="ville_utlstr">Ville</label>
                        <input class="form-control" type="text" id="ville_utlstr" name="ville_utlstr">
                    </div>
                </div>
                <div class="mt-4">
                    <input class="btn btn-primary btn-lg ms-auto" type="submit" value="S'inscrire">
                </div>
            </div>

        </form>

        

        <!-- Footer Start -->
        <div class="container-fluid bg-img text-secondary" style="    margin-top: 132px;">
            <div class="container">
                <div class="row gx-5">
                    <div class="col-lg-4 col-md-6 mb-lg-n5">
                        <div class="d-flex flex-column align-items-center justify-content-center text-center h-100 bg-primary border-inner p-4">
                            <a href="index.html" class="navbar-brand">
                                <h1 class="m-0 text-uppercase text-white"><i class="fa fa-birthday-cake fs-1 text-dark me-3"></i>ELYA'S DONUTS</h1>
                            </a>
                            <p class="mt-3">Lorem diam sit erat dolor elitr et, diam lorem justo labore amet clita labore stet eos magna sit. Elitr dolor eirmod duo tempor lorem, elitr clita ipsum sea. Nonumy rebum et takimata ea takimata amet gubergren, erat rebum magna lorem stet eos. Diam amet et kasd eos duo dolore no.</p>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <div class="row gx-5">
                            <div class="col-lg-4 col-md-12 pt-5 mb-5">
                                <h4 class="text-primary text-uppercase mb-4">ENTRER EN CONTACT</h4>
                                <div class="d-flex mb-2">
                                    <i class="bi bi-geo-alt text-primary me-2"></i>
                                    <p class="mb-0">123 Street, New York, USA</p>
                                </div>
                                <div class="d-flex mb-2">
                                    <i class="bi bi-envelope-open text-primary me-2"></i>
                                    <p class="mb-0">info@example.com</p>
                                </div>
                                <div class="d-flex mb-2">
                                    <i class="bi bi-telephone text-primary me-2"></i>
                                    <p class="mb-0">+33 345 67890</p>
                                </div>
                                <div class="d-flex mt-4">
                                    <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-twitter fw-normal"></i></a>
                                    <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-facebook-f fw-normal"></i></a>
                                    <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-linkedin-in fw-normal"></i></a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                                <h4 class="text-primary text-uppercase mb-4">LIENS RAPIDES</h4>
                                <div class="d-flex flex-column justify-content-start">

                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Dashboard</a>
                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Commandes</a>
                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Rechercher</a>
                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>catalogue</a>
                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Enregistrer</a>
                                    <a class="text-secondary" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Contact Us</a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                                <h4 class="text-primary text-uppercase mb-4">Newsletter</h4>
                                <p>Amet justo diam dolor rebum lorem sit stet sea justo kasd</p>
                                <form action="">
                                    <div class="input-group">
                                        <input type="text" class="form-control border-white p-3" placeholder="Your Email">
                                        <button class="btn btn-primary">Sign Up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid text-secondary py-4" style="background: #111111;">
            <div class="container text-center">
                <p class="mb-0">&copy; <a class="text-white border-bottom" href="#">Your Site Name</a>. All Rights Reserved.


            </div>
        </div>
        <!-- Footer End -->


























          <!-- JavaScript Libraries -->
          <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/waypoints/waypoints.min.js"></script>
        <script src="lib/counterup/counterup.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
</body>

</html>