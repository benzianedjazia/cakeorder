<?php
session_start();
/*
 * Chargement de l'autoloader afin de disposer 
 */
require_once '../libraries/autoloader_.php';

if (isset($_SESSION['access_ctrl']) && $_SESSION['access_ctrl'] == hash("sha256", session_id() . "913746")) :
?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <meta charset="UTF-8">
        <title>cakeorder.fr</title>
        <base href="http://www.cakeorderold.fr">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.13.0/jquery-ui.min.js" integrity="sha256-hlKLmzaRlE8SCJC1Kw8zoUbU8BxA+8kR3gseuKfMjxA=" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="../../assets/style.css">

        <!-- Favicon -->
        <link href="img/favicon.ico" rel="icon">

        <!-- Google Web Fonts -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:wght@400;600&family=Oswald:wght@500;600;700&family=Pacifico&display=swap" rel="stylesheet">

        <!-- Icon Font Stylesheet -->



        <!-- Libraries Stylesheet -->
        <link href="../../assets/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        <!-- PERSONNALISÉized Bootstrap Stylesheet -->
        <link href="../../assets/css/bootstrap.min.css" rel="stylesheet">

        <!-- Template Stylesheet -->
        <link href="../../assets/css/style.css" rel="stylesheet">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.4.1/font/bootstrap-icons.css" rel="stylesheet">
        <script src="../../assets/js/jquery.min.js"></script>
        <script src="https://code.jquery.com/ui/1.13.1/jquery-ui.min.js" integrity="sha256-eTyxS0rkjpLEo16uXTS0uVCS4815lc40K2iVpWDvdSY=" crossorigin="anonymous"></script>    </head>
    <style>
        html,
        body {
            width: 100%;
            min-height: 100%;
        }

        main {
            min-width: 100%;
            min-height: 100%;
        }

        #ctn_div_central {
            width: 100%;
            padding: 0px;
        }

        section[id^="sctn_tab_"] {
            width: 100%;
            padding: 0px 80px;
            margin: 40px 0px;
        }

        /* .ms-auto {
                    /* margin-right: 50px !important;
                }*/

        nav.mr-2 {
            margin-right: 20px !important;
        }


        /* 
                .dsply-no{
                   display: none;
                            }*/

        #ss_ctn_from {
            display: table;
            width: 100%;
            border-spacing: 25px;
        }

        #ss_ctn_from_left {
            display: table-cell;
            width: 50%;
        }

        .dsply-no {
            display: none !important;
        }

        #slct_cvlt {
            width: 145px;
        }

        input[type=date] {
            width: 150px;
        }

        .header {
            background-color: #100f10 !important;
            box-shadow: 1px 1px 3px #0c0b0d94;
        }



        #ctn_form_search {
            width: 500px !important;
        }

        #output {
            width: 100% !important;
        }

        /* configuration des fenêtres modales */
        .modal-header {
            padding: 0.5rem 0.5rem !important;
        }

        /*Réajustement de positionnement de la fenêtre modal au centre */
        #dialogInfo .modal-dialog {
            position: absolute;
            width: 100% !important;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
        }

        /*Ajustement modal-body*/

        .modal-body {
            padding: 2rem 1rem !important;
        }



        /*Ajustement sur le tableau de résultat*/

        #tbl_result tbody tr:hover {
            background-color: #fdd;
            transition-delay: 0s;
            transition-duration: 1s;
            transition-property: all;
        }

        #tbl_result tr {
            text-align: center;
            cursor: pointer;
        }


        /*Surcharge de la classe active*/
        .active {
            background-color: #bd0dfd !important;
        }

        #srch {

            display: none;


        }

        #ss_ctn_from legend {
            text-align: center;
            text-transform: uppercase;
        }

        /**/
        .form-control:focus {
            box-shadow: none !important;
            border-color: silver;
        }

        /**/
        button.btn:focus {
            box-shadow: none !important;
            border-color: silver;
        }


        /*Ajust first li navbar*/

        ul.navbar-nav:first-child {
            padding: 0.65rem !important;
        }

        .navbar {
            padding: 0px;
        }

        /*Ajustement du ctn li de session*/
        #no_load {
            margin-left: 10px !important;
            width: 160px !important;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function() {

            $("#no_load").click(function() {
                $("#menu_prcpl").slideToggle();
            })



            $("#srch1").click(function() {
                $("#srch").css("display", "block");
            })
            /*===================================================
             *    Fonction de soumission des données formulaire
             ===================================================*/
            var ajaxOptions = {
                method: "POST",
                cache: false,
                async: true,
                timeout: 3000,
                dataType: "json",
                processData: false,
                contentType: false
            };

            /***********************************************************************
             *              Fonction reset form inscription
             ***********************************************************************/
            function reset_form() {
                $("#form_inscription")[0].reset();
                //$("#bt_submit").prop({disabled: true});
            }


            let launchDialogInfo = function(elemetText) {
                $("#dialogInfo div.modal-body").html(elemetText);
                $("#btn_show_dialog_info").click();
            };

            /*
             | -------------------------------------------------------------------
             |  Fonction inscription : client
             | -------------------------------------------------------------------
             | Définition:
             | Assure l'enregistrement des données
             |
             */
            function inscriptionClient() {
                //Récupération du formulaire
                var form = $("#form_inscription").get(0);
                //Lecteur de la valeur de l'attribut action du formulaire
                var url = form.getAttribute("action");
                var formData = new FormData(form);
                ajaxOptions.data = formData;
                ajaxOptions.url = url;
                $.ajax(ajaxOptions).done(function(clbck) {
                    if (clbck.err_flag) {
                        alert(clbck.err_msg);
                    } else {
                        let flag_response = "Les donnée ont été enregistrées!";
                        launchDialogInfo(flag_response);
                        reset_form();
                    }
                    dis_panel_flw();
                }).fail(function(e) {
                    console.log(e);
                    alert("Error!");
                }).always(function() {
                    //dis_panel_flw();
                });
            }
            var ajaxOptions = {
                method: "POST",
                cache: false,
                async: true,
                timeout: 5000,

                processData: false,
                contentType: false
            };

            // $("#photo_donut_btn").click(function(e) {
            //     e.preventDefault();
            //     $("#photo_donut_inpt").click();
            // });

            // $("#denomination_donut_inpt").change(function(e) {
            //     alert($(this).val());
            // })
            $("#photo_donut_inpt").change(function(e) {
                // for(let index in this.files){
                //     alert(this.files[index]);
                // }
                for (let cpt = 0; cpt < this.files.length; cpt++) {
                    let file = this.files[cpt];
                    let form = document.querySelector("#form_cake");
                    if (typeof window.FileReader != "undefined") {
                        let fr = new FileReader();
                        fr.onload = function(event) {

                            let src = event.target.result;
                            $("#output").append("<figure class='figure'><img src='" + src + "' class='img-thumbnail me-1'></figure>");
                            $("#photo_donut_img").attr({
                                // "src": src

                            });
                            let Fd = new FormData(form);
                            //   Fd.append("denomination_donut_inpt",$("#denomination_donut_inpt").val()); en cas ya pas le form data 
                            formDataFile.append('photo', file)
                            ajaxOptions.url = form.getAttribute("action");
                            ajaxOptions.data = Fd;
                            $.ajax(ajaxOptions).done(function(clbck) {

                            }).fail(function(e) {
                                // console.log(e);
                                // alert("Error!");
                            }).always(function() {
                                //dis_panel_flw();
                            });
                        };
                        fr.onerror = function() {
                            alert("flag_err");
                        };
                        fr.readAsDataURL(file);


                    } else {
                        alert("l'API FileReader N'est pas active!  ")
                    }
                }
            })
            $("#btn_reset").click(function() {
                $("input[type='text'],input[type='file]").each(function(index) {
                    $(this).val("");
                    $("output figure").remove("figure.figure");
                });
            });

            function formcake() {
                //Récupération du formulaire
                var form = $("#form_cake").get(0);
                //Lecteur de la valeur de l'attribut action du formulaire
                var url = form.getAttribute("action");
                var formData = new FormData(form);
                ajaxOptions.data = formData;
                ajaxOptions.url = url;
                $.ajax(ajaxOptions).done(function(clbck) {
                    if (clbck.err_flag) {
                        alert(clbck.err_msg);
                    } else {
                        let flag_response = "Les donnée ont été enregistrées!";
                        launchDialogInfo(flag_response);
                        reset_form();
                    }
                    dis_panel_flw();
                }).fail(function(e) {
                    console.log(e);
                    alert("Error!");
                }).always(function() {
                    //dis_panel_flw();
                });
            }

            function dashbord() {
                //Récupération du formulaire
                var form = $("#dashbord").get(0);
                //Lecteur de la valeur de l'attribut action du formulaire
                var url = form.getAttribute("action");
                var formData = new FormData(form);
                ajaxOptions.data = formData;
                ajaxOptions.url = url;
                $.ajax(ajaxOptions).done(function(clbck) {
                    if (clbck.err_flag) {
                        alert(clbck.err_msg);
                    } else {
                        let flag_response = "Les donnée ont été enregistrées!";
                        launchDialogInfo(flag_response);
                        reset_form();
                    }
                    dis_panel_flw();
                }).fail(function(e) {
                    console.log(e);
                    alert("Error!");
                }).always(function() {
                    //dis_panel_flw();
                });
            }
            /*
             | -------------------------------------------------------------------
             |  Fonction de effacement : client
             | -------------------------------------------------------------------
             | Définition:
             |
             | Assure la ré-initialisation du tableau
             |
             */
            let resetBodyTableResultSearch = function() {
                $("tbody[id='tbody_result'] tr").remove(".ajust");
                $("tbody[id='tbody_result'] td").remove(".ajust");
            };

            var searchClient = function() {
                resetBodyTableResultSearch();
                var formSearch = $("#form_search")[0];
                var url = formSearch.getAttribute('action');
                if (parseInt(formSearch.elements["keyword"].value.length) >= 4) {
                    var formData = new FormData(formSearch);
                    ajaxOptions.data = formData;
                    ajaxOptions.url = url;
                    ajaxOptions.dataType="json";
                    $.ajax(ajaxOptions).done(function(tab_acheteurs) {
                        resetBodyTableResultSearch();
                        if (tab_acheteurs) {
                            let response = "";
                            $("#tbl_result").removeClass("dsply-no").fadeIn("2000", function() {
                                let dateAnniv = null;
                                for (var index in tab_acheteurs) {
                                    //tab_acheteurs=JSON.parse(tab_acheteurs);
                                //    console.log(tab_acheteurs);
                                    //  $.datepicker.setDefaults($.datepicker.regional["fr"]);
                                    //  dateAnniv = $.datepicker.formatDate('dd-mm-yy', new Date(tab_acheteurs[index][dateAnniversaire]));
                                  
                                    response += "<tr class='ajust'>\n\
                                                    <td>" + (parseInt(index) + 1) + "</td>\n\
                                                    <td>" + tab_acheteurs[index].nom + "</td>\n\
                                                    <td>" + tab_acheteurs[index].prenom + "</td>\n\
                                                    <td>" + tab_acheteurs[index].email + "</td>\n\
                                                    <td>" + tab_acheteurs[index].telephone + "</td>\n\
                                                    <td>"+ tab_acheteurs[index].dateAnniversaire+ "</td>\n\
                                                </tr>";
                                }
                                $("tbody[id='tbody_result']").append(response);
                            });
                        } else {
                            launchDialogInfo("Aucun(s) résultat(s) trouvé(s)!");
                        }
                    }).fail(function() {
                        alert("Une erreur est survenue lors de l'envoie des données!");
                    }).always(function() {

                    });
                } else {
                    launchDialogInfo("Votre saisie n'est pas correcte!");
                }
            };


            //                $("#bt_act_inscription").click(function (e) {
            //                    e.preventDefault();
            //                    ajaxFonction();
            //                });

            /***********************************************************************
             *              Fonction de soumission des données formulaire
             ***********************************************************************/
            /**
             * 
             */
            $("#form_search").submit(function(e) {
                e.preventDefault();
                searchClient();

            });

            $("").click(function(e) {
                e.preventDefault();
                $("#dashbord").css("display", "block")
                dashbord();

            });

            /*===================================================
             *    Soumission des données formulaire inscription
             ===================================================*/
            $("#form_inscription").submit(function(e) {
                e.preventDefault();
                inscriptionClient();

            });
            $("#form_cake").submit(function(e) {
                e.preventDefault();
                formcake();

            });


            //                    function select_content_sctn(id_tab) {
            //                        $("section[id^='sctn_tab_']").stop(true, true).fadeOut("fast", function (e) {
            //                            $(this).css({"display": "none"});
            //                            $("section[id='sctn_tab_" + id_tab + "']").stop(true, true).fadeIn();
            //                        });
            //                    }


            function select_content_sctn(id_tab) {
                $("section[id^='sctn_tab_']").addClass("dsply-no").stop(true, true).fadeOut("fast", function(e) {
                    $("section[id='sctn_tab_" + id_tab + "']").stop(true, true).removeClass("dsply-no").fadeIn();
                });
            }

            $(" nav li.nav-item:not(li[id='no_load'])").click(function(e) {
                e.preventDefault();
                let id_tab = $(this).attr("data-sctn-id");
                select_content_sctn(id_tab);
            });

            /*===================================================
             *    Soumission des données formulaire inscription
             ===================================================*/
            $("li a:not(a[id='navbarDropdown'])").click(function(e) {
                e.preventDefault();
                $("li a").removeClass("active");
                $(this).addClass("active");
                if ($(this).hasClass("active")) {
                    $("li a").removeClass("active");
                    $(this).addClass("active");
                } else {
                    $("li a").removeClass("active");
                    $(this).addClass("active");
                }
            });



            /*===================================================
             *    Logout  : Déconnexion de l'application
             ===================================================*/
            $("li[id='btn_actn_logout'] ").click(function(e) {
                e.preventDefault();
                $.ajax({
                    url: "http://www.cakeorderold.fr/application/controllers/compte.php",
                    method: "POST",
                    data: {
                        "action": "logout"
                    }
                }).done(function(flag_rsp) {
                    window.location.replace("http://www.cakeorderold.fr");
                }).fail(function() {
                    launchDialogInfo("Une erreur est survenue lors de la requête de deconnexion!");
                }).always(function() {

                });
            });



        });
    </script>
    </head>

    <body>
        <!-- Topbar Start -->
        <div id="topbar" class="container-fluid px-0 d-none d-lg-block">
            <div class="row gx-0">
                <div class="col-lg-4 text-center bg-secondary py-3">
                    <div class="d-inline-flex align-items-center justify-content-center">
                        <i class="bi bi-envelope fs-1 text-primary me-3"></i>
                        <div class="text-start">
                            <h6 class="text-uppercase mb-1">ENVOYEZ-NOUS UN EMAIL</h6>
                            <span>info@example.com</span>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 text-center bg-primary border-inner py-3">
                    <div class="d-inline-flex align-items-center justify-content-center">
                        <a href="index.html" class="navbar-brand">
                            <h1 class="m-0 text-uppercase text-white"><img src="../../assets/img/img - Copie/elyasdonutslogo.jpg" width="50px" style="transform:translate(-50px,-5px) ;border-radius:50% ;" alt=""></i>ELYA'S DONUTS</h1>
                        </a>
                    </div>
                </div>
                <div class="col-lg-4 text-center bg-secondary py-3">
                    <div class="d-inline-flex align-items-center justify-content-center">
                        <i class="bi bi-phone-vibrate fs-1 text-primary me-3"></i>
                        <div class="text-start">
                            <h6 class="text-uppercase mb-1">APPELEZ-NOUS</h6>
                            <span>+012 345 6789</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Topbar End -->
        <div id="ctn_div_central">
            <header class="navbar navbar-expand-md navbar-dark header">
                <section class="navbar-brand">

                    <img src="../../assets/img/img - Copie/elyasdonutslogo.jpg" width="60px" style="border-radius:50% ;" alt="">
                    <span id="" class="narbar-text ml-3">ELYA'S DONUTS</span>
                </section>
                <section class="navbar ms-auto ">
                    <nav class="mr-1">
                        <ul class="navbar-nav nav-pills justify-content-center ">
                            <li data-sctn-id="1" class="nav-item" id="dashbord1">
                                <a class="nav-link " href="#">Dashboard</a>
                            </li>
                            <li data-sctn-id="2" class="nav-item">
                                <a class="nav-link " href="#">Commandes</a>
                            </li>
                            <li data-sctn-id="3" class="nav-item">
                                <a class="nav-link " href="#">Enregistrer</a>
                            </li>

                            <li data-sctn-id="5" class="nav-item">
                                <a class="nav-link " href="#">catalogue</a>

                            </li>
                            <li data-sctn-id="4" class="nav-item">
                                <a class="nav-link " href="#">recharche</a>

                            </li>

                            <li class="nav-item dropdown" id="no_load">
                                <!-- Il faut retirer la classe .dropdown-toggle de a pour enlever > -->
                                <a class="nav-link bg-light  " href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" data-bs-target="#menu_prcpl" aria-expanded="false">
                                    <span class="bg-light text-black p-1 rounded"><?= $_SESSION["admin_name"]; ?></span>
                                </a>
                                <ul id="menu_prcpl" class="dropdown-menu dropdown" aria-labelledby="navbarDropdown">
                                    <li><a class="dropdown-item" href="#">Paramètres</a></li>
                                    <li id="btn_actn_logout"><a class="dropdown-item" href="#">Déconnexion</a></li>
                                </ul>
                            </li>
                        </ul>

                    </nav>
                </section>
            </header>

        </div>
        </header>
        <main>
            <section class="dsply-no " data-sctn-id="3" id="sctn_tab_3">
                <form id="form_inscription" action="http://www.cakeorderold.fr/application/controllers/inscription.php" method="post" enctype="application/x-www-form-urlencoded">
                    <div id="ss_ctn_from">
                        <input type="hidden" name="action" value="inscription">
                        <div id="ss_ctn_from_left" class="p-3 bg-light border  rounded">
                            <legend class="border-bottom " style="width: 100%;">Données client</legend>
                            <div class="mb-2">
                                <label class="form-label" for="nom_utlstr">Civilité</label>
                                <select class="form-select" id="slct_cvlt">
                                    <option selected="selected">--- Choisir ---</option>
                                    <option value="masculin">Mr</option>
                                    <option value="feminin">Mme</option>
                                </select>
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="nom_utlstr">Nom</label>
                                <input class="form-control" type="text" id="nom_utlstr" name="nom_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="prenom_utlstr">Prénom</label>
                                <input class="form-control" type="text" id="prenom_utlstr" name="prenom_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="email_utlstr">Email</label>
                                <input class="form-control" type="text" id="email_utlstr" name="email_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="telephone_utlstr">Téléphone</label>
                                <input class="form-control" type="text" id="telephone_utlstr" name="telephone_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="dateAnniversaire_utlstr">Date anniversaire</label>
                                <input class="form-control" type="date" id="dateAnniversaire_utlstr" name="dateAnniversaire_utlstr">
                            </div>
                            </fieldset>
                        </div>
                        <div id="ss_ctn_from_right" class="p-3 bg-light border  rounded">
                            <legend class="border-bottom">Adresse client</legend>
                            <div class="mb-2">
                                <label class="form-label" for="numVoie_utlstr">Numero de voie</label>
                                <input class="form-control" type="text" id="numVoie_utlstr" name="numVoie_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="libelleVoie_utlstr">Libelle de voie</label>
                                <input class="form-control" type="text" id="libelleVoie_utlstr" name="libelleVoie_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="codePostal_utlstr">Code postal</label>
                                <input class="form-control" type="text" id="codePostal_utlstr" name="codePostal_utlstr">
                            </div>
                            <div class="mb-2">
                                <label class="form-label" for="ville_utlstr">Ville</label>
                                <input class="form-control" type="text" id="ville_utlstr" name="ville_utlstr">
                            </div>
                        </div>
                        <div class="mt-4">
                            <input class="btn btn-primary btn-lg ms-auto" type="submit" value="S'inscrire">
                        </div>
                    </div>

                </form>
               
                <section class="dsply-no " data-sctn-id="5" id="sctn_tab_5">
                    <form id="form_cake" action="http://www.cakeorderold.fr/application/controllers/donut.php" method="post" enctype="application/x-www-form-urlencoded">
                        <div id="ss_ctn_from">
                            <input type="hidden" name="action" value="inscription">
                            <div id="ss_ctn_from_left" class="p-3 bg-light border  rounded">
                                <legend class="border-bottom " style="width: 100%;">ALL catalogue</legend>


                                <div class="mb-2">
                                    <label class="form-label" for="référence"> référence</label>
                                    <input class="form-control" type="text" id="référence" name="référence">
                                </div>
                                <div class="mb-2">
                                    <label class="form-label" for="dénomination">dénomination</label>
                                    <input class="form-control" type="text" id="dénomination" name="dénomination">
                                </div>
                                <div class="mb-2">
                                    <label class="form-label" for="description"> description</label>
                                    <textarea class="form-control" name="description" id="description" cols="10" rows="10" resize="none"></textarea>

                                </div>
                                </fieldset>
                            </div>
                            <div id="ss_ctn_from_right" class="p-3 bg-light border  rounded">
                                <legend class="border-bottom">détailler cake</legend>
                                <div class="mb-2">
                                    <label class="form-label" for="prixUnitaire">prixUnitaire</label>
                                    <input class="form-control" type="text" id="prixUnitaire" name="prixUnitaire">
                                </div>

                                <div>
                                    <label for="photo_donut_inpt">
                                        <img src="" id="photo_donut_img" alt="PHOTO DONUT">
                                    </label>
                                    <!-- <button id="photo_donut_btn">
                    <img src="img/photo-64.png" alt="[img/bouton]" >
                </button> -->
                                    <input type="file" name="photo_donut[]" id="photo_donut_inpt" multiple accept="image/png,image/jpeg">
                                    <output id="output"></output>
                                </div>

                            </div>
                            <div class="mt-4">
                                <input class="btn btn-primary btn-lg ms-auto" type="submit" value="S'inscrire">
                            </div>
                        </div>

                    </form>
                </section>
        </main>
        </div>
        <!-- Start Popupmodal infos -->
        <span id="btn_show_dialog_info" data-bs-toggle="modal" data-bs-target="#dialogInfo"></span>
        <div id="dialogInfo" class="modal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-light">
                        <div class="lead modal-title">Informations</div>
                        <button type="button" class="btn-close" data-bs-dismiss="modal"></button>
                    </div>
                    <div class="modal-body"></div>
                </div>
            </div>
        </div>
        <!-- Stop Popupmodal infos -->
        <!------------------------------------------------ maine dashboard  ------------------------------------------->


        <section class="dsply-no " style="    transform: translatey(50px); margin-bottom: -286px; " data-sctn-id="5" id="sctn_tab_5">
            <div class="section-title position-relative text-center mx-auto mb-5 pb-3" style="max-width: 600px;">
                <h2 class="text-primary font-secondary">Menus & Tarifs</h2>
                <h1 class="display-4 text-uppercase">DÉCOUVREZ NOS GÂTEAUX</h1>
            </div>
            <div class="row g-3 ">
                <div class=" card mb-3 ms-1 " style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="../../assets/img/img - Copie/fraise.jpg" class="img-fluid rounded-start" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="../../assets/img/img - Copie/ph1.jpg" class="img-fluid rounded-start" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>

            <div class="row g-3">
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="../../assets/img/img - Copie/ph3.jpg" class="img-fluid rounded-start" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-3" style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="../../assets/img/img - Copie/ph4.jpg" class="img-fluid rounded-start" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row g-2">
                <div class="card mb-2" style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="../../assets/img/img - Copie/ph5.jpg" class="img-fluid rounded-start" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-2" style="max-width: 540px;">
                    <div class="row g-0">
                        <div class="col-md-4">
                            <img src="../../assets/img/img - Copie/ph2.jpg" class="img-fluid rounded-start" alt="...">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <h5 class="card-title">Card title</h5>
                                <p class="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </section>

        <section class="dsply-no " data-sctn-id="4" id="sctn_tab_4">
                    <div id="ctn_form_search">
                        <form name="form_search" id="form_search" action="http://www.cakeorderold.fr/application/controllers/client.php" method="POST" enctype="application/x-www-form-urlencoded">
                            <input type="hidden" name="action" value="rechercher">
                            <div class="input-group">
                                <input class="form-control w-25 " type="search" id="keyword" name="keyword">
                                <button class="btn btn-secondary btn-lg">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                        <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z" />
                                    </svg>
                                </button>
                            </div>
                        </form>
                    </div>
                    <output id="output" class="mt-4">
                        <table id="tbl_result" class="table table-striped ">
                            <thead>
                                <tr>
                                    <th>N°</th>
                                    <th>Nom</th>
                                    <th>Prénom</th>
                                    <th>Email</th>
                                    <th>Téléphone</th>
                                    <th>Anniversaire</th>
                                </tr>
                            </thead>
                            <tbody id="tbody_result">

                            </tbody>
                        </table>
                        <div></div>
                    </output>
                </section>
        <!-- Footer Start -->
        <div class="container-fluid bg-img text-secondary" style="    margin-top: 400px;">
            <div class="container">
                <div class="row gx-5">
                    <div class="col-lg-4 col-md-6 mb-lg-n5">
                        <div class="d-flex flex-column align-items-center justify-content-center text-center h-100 bg-primary border-inner p-4">
                            <a href="index.html" class="navbar-brand">
                                <h1 class="m-0 text-uppercase text-white"><i class="fa fa-birthday-cake fs-1 text-dark me-3"></i>ELYA'S DONUTS</h1>
                            </a>
                            <p class="mt-3">Lorem diam sit erat dolor elitr et, diam lorem justo labore amet clita labore stet eos magna sit. Elitr dolor eirmod duo tempor lorem, elitr clita ipsum sea. Nonumy rebum et takimata ea takimata amet gubergren, erat rebum magna lorem stet eos. Diam amet et kasd eos duo dolore no.</p>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-6">
                        <div class="row gx-5">
                            <div class="col-lg-4 col-md-12 pt-5 mb-5">
                                <h4 class="text-primary text-uppercase mb-4">ENTRER EN CONTACT</h4>
                                <div class="d-flex mb-2">
                                    <i class="bi bi-geo-alt text-primary me-2"></i>
                                    <p class="mb-0">123 Street, New York, USA</p>
                                </div>
                                <div class="d-flex mb-2">
                                    <i class="bi bi-envelope-open text-primary me-2"></i>
                                    <p class="mb-0">info@example.com</p>
                                </div>
                                <div class="d-flex mb-2">
                                    <i class="bi bi-telephone text-primary me-2"></i>
                                    <p class="mb-0">+33 345 67890</p>
                                </div>
                                <div class="d-flex mt-4">
                                    <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-twitter fw-normal"></i></a>
                                    <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-facebook-f fw-normal"></i></a>
                                    <a class="btn btn-lg btn-primary btn-lg-square border-inner rounded-0 me-2" href="#"><i class="fab fa-linkedin-in fw-normal"></i></a>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                                <h4 class="text-primary text-uppercase mb-4">LIENS RAPIDES</h4>
                                <div class="d-flex flex-column justify-content-start">

                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Dashboard</a>
                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Commandes</a>
                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Rechercher</a>
                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>catalogue</a>
                                    <a class="text-secondary mb-2" href="#"><i class="bi bi-arrow-right text-primary me-2"></i>Enregistrer</a>

                                </div>
                            </div>
                            <div class="col-lg-4 col-md-12 pt-0 pt-lg-5 mb-5">
                                <h4 class="text-primary text-uppercase mb-4">Newsletter</h4>
                                <p>Amet justo diam dolor rebum lorem sit stet sea justo kasd</p>
                                <form action="">
                                    <div class="input-group">
                                        <input type="text" class="form-control border-white p-3" placeholder="Your Email">
                                        <button class="btn btn-primary">Sign Up</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid text-secondary py-4" style="background: #111111;">
            <div class="container text-center">
                <p class="mb-0">&copy; <a class="text-white border-bottom" href="#">Your Site Name</a>. All Rights Reserved.


            </div>
        </div>
        
        <!-- Footer End -->































        <!-- JavaScript Libraries -->
        <script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"></script>
        <script src="lib/easing/easing.min.js"></script>
        <script src="lib/waypoints/waypoints.min.js"></script>
        <script src="lib/counterup/counterup.min.js"></script>
        <script src="lib/owlcarousel/owl.carousel.min.js"></script>

        <!-- Template Javascript -->
        <script src="js/main.js"></script>
    </body>

    </html>

<?php
else :
    \Route::defaultRedirection();
endif;
?>