<?php

/**
 *
 * @author  GRICOLAT Didier
 */

namespace application\libraries;

trait loaderFile {

    private static $_nbOfValue = 8;
    public static  function getRandomName(){

        return substr((md5(uniqid(rand(10000,100000),true) )),0,20) ;
      }
    public function generateIdWithParams($nbOfValues) {
        $values = [];
        for ($cpt = 0; $cpt < $nbOfValues; $cpt++) {
            $values [] = strval(rand(1, 9));
        }
        return implode(",", $values);
    }

    public function generateId() {
        $values = [];
        for ($cpt = 0; $cpt < self::$_nbOfValue ; $cpt++) {
            $values [] = strval(rand(1, 9));
        }
        return implode(",", $values);
    }

}
