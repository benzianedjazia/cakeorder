<?php

//namespace application\config;

/**
 * Description of Route
 *
 * @author GRICOLAT Didier
 */
class Route {

    const DEFAULT_VIEW = "http://www.cakeorderold.fr/application/views/home.php";
    const DEFAULT_SCHEME_VIEWS = "http://www.cakeorderold.fr/application/views/";

    function __construct($route = "default") {
        if (isset($route)) :
            switch ($route) :
                case "default":
                    $this->defaultRedirection();
                    break;
                case "":

                    break;
                default:

                    break;
            endswitch;
        endif;
    }

    static function defaultRedirection() {
        session_destroy();
        header("Location:" . self::DEFAULT_VIEW);
    }

    static function redirection($page) {
        header("Location:" . self::DEFAULT_SCHEME_VIEWS . $page);
    }

    function getDefaulRoad() {
        return $this->_defaulRoad;
    }

    function setDefaulRoad($defaulRoad){
        $this->_defaulRoad = $defaulRoad;
    }

}
